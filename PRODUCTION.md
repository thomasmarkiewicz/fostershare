# PRODUCTION

## Kubernetes

Use Kubernetes declaritively as much as possible.

This means describing the desired state of the cluster in configuration files under 'k8s' subdirectly and allowing kubectl to figure out how to get to that state. The alternative is to issue imerative commands which are cumbersome and easily lost.

Describe each Kubernetes OBJECT in a separate file.

### minikube

Frequently used minikube commands:

```
minikube start
minikube status
minikube ip
minikube stop
```

### kubectl

Frequently used kubectl commands:

```
kubectl cluster-info
kubectl config current-context
kubectl config get-contexts
kubectl config use-context minikube
kubectl config use-context do-nyc3-k8s-fostershare

```

### Digital Ocean

The Kubernetes cluster for this project runs in Digital Ocean.

For maintainers only, to [connect to your local custer](https://www.digitalocean.com/docs/kubernetes/how-to/connect-to-cluster/):

- [Install doctl](https://github.com/digitalocean/doctl#downloading-a-release-from-github) on your dev machine

- create/find your Digital Ocean API access token here

```
https://cloud.digitalocean.com/account/api/tokens
```

- for convencience, export it in an env variable on you dev machine (for example add the following to .bashrc on linux)

```
export DIGITALOCEAN_ACCESS_TOKEN=<your_token>
```

- authenticate with Digital Ocean

```
doctl auth init
```

- connect your local kubectl to Digital Ocean cluster

```
doctl kubernetes cluster kubeconfig save k8s-fostershare
```

- verify kubectl points to Digital Ocean cluster: `do-nyc3-k8s-fostershare`

```
kubectl config current-context
```

- you can now use kubectl to control your `do-nyc3-k8s-fostershare` cluster

- to switch clusters

```
kubectl config get-contexts
kubectl config use-context minikube
kubectl config use-context do-nyc3-k8s-fostershare
```

#### Build and tag new production Docker images

See what the previous server-gql version was here:
```
k8s/server-gql-deployment.yml
```
Then build the image
```
cd server-gql
docker build -t thomasmarkiewicz/fostershare-server-gql:0.0.2 .
docker build -t thomasmarkiewicz/fostershare-server-gql .
```
Login and upload to docker hub:
```
docker login --username=thomasmarkiewicz
docker push thomasmarkiewicz/fostershare-server-gql:0.0.2
docker push thomasmarkiewicz/fostershare-server-gql
```

Do the same for webapp.  Check latest version in
```
k8s/webapp-deployment.yml
```

Build webapp prod images
```
cd webapp
docker build -t thomasmarkiewicz/fostershare-webapp:0.0.3 .
docker build -t thomasmarkiewicz/fostershare-webapp .
docker push thomasmarkiewicz/fostershare-webapp:0.0.3
docker push thomasmarkiewicz/fostershare-webapp
```

Finally update k8s versions and push updates to DigitalOcean kubernetes cluster
```
vim k8s/server-gql-deployment.yml
vim k8s/server-webapp-deployment.yml

kubectl config use-context do-nyc3-k8s-fostershare
kubectl apply -f k8s
```

#### Deploy database updates to Digital Ocean

Shut down your local dev environment (so that nothing is running on port 4466)

Setup port-forwarding to DO via kubectl:

```
kubectl port-forward svc/server-prisma-cluster-ip-service 4466:4466
```

Verify you can access it in your local browser

```
http://localhost:4466
```

Optionally delete all database data (ONLY BEFORE LAUNCH):
```
cd server-gql
prisma reset -f
```

OR all database schema AND data (ONLY BEFORE LAUNCH):
```
cd server-gql
prisma delete
```

Deploy prisma updates
```
cd server-gql/prisma
prisma deploy
```

To seed the database with new test data:
```
cd server-gql
npm run db:seed
```



#### Manually deploy new images to Digital Ocean cluster

```
kubectl config use-context do-nyc3-k8s-fostershare
kubectl apply -f k8s
```

#### Troubleshooting

see the logs of the pod deployed in k:
```
kubectl logs server-gql-deployment-86f7d7f445-9r7cf
```

ssh into a docker container
```
docker run -it thomasmarkiewicz/fostershare-server-gql sh
```

## Resources

- https://www.digitalocean.com/community/tutorials/how-to-install-software-on-kubernetes-clusters-with-the-helm-package-manager

```
kubectl -n kube-system create serviceaccount tiller
kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller
kubectl get pods --namespace kube-system

```

- https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes

Create mandatory Nginx Ingress Controller resources:

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.24.1/deploy/mandatory.yaml

namespace/ingress-nginx created
configmap/nginx-configuration created
configmap/tcp-services created
configmap/udp-services created
serviceaccount/nginx-ingress-serviceaccount created
clusterrole.rbac.authorization.k8s.io/nginx-ingress-clusterrole created
role.rbac.authorization.k8s.io/nginx-ingress-role created
rolebinding.rbac.authorization.k8s.io/nginx-ingress-role-nisa-binding created
clusterrolebinding.rbac.authorization.k8s.io/nginx-ingress-clusterrole-nisa-binding created
deployment.apps/nginx-ingress-controller created
```

Create DigitalOcean LoadBalancer Service

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.24.1/deploy/provider/cloud-generic.yaml

service/ingress-nginx created
```

Confirm

```
kubectl get svc --namespace=ingress-nginx

NAME            TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)                      AGE
ingress-nginx   LoadBalancer   10.245.156.220   159.89.253.211   80:32564/TCP,443:31365/TCP   98s

```

Create the cert-manager Custom Resource Definitions (CRDs)

```
kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.8/deploy/manifests/00-crds.yaml

customresourcedefinition.apiextensions.k8s.io/certificates.certmanager.k8s.io created
customresourcedefinition.apiextensions.k8s.io/challenges.certmanager.k8s.io created
customresourcedefinition.apiextensions.k8s.io/clusterissuers.certmanager.k8s.io created
customresourcedefinition.apiextensions.k8s.io/issuers.certmanager.k8s.io created
customresourcedefinition.apiextensions.k8s.io/orders.certmanager.k8s.io created

```

Add a label

```
kubectl label namespace kube-system certmanager.k8s.io/disable-validation="true"
```

Add Helm chart

```
helm repo add jetstack https://charts.jetstack.io
helm install --name cert-manager --namespace kube-system jetstack/cert-manager --version v0.8.0
```

Add staging ClusterIssuer

```
kubectl create -f staging_issuer.yaml
```

- https://www.prisma.io/tutorials/deploy-prisma-to-kubernetes-ct13

In the above link, skip the first half of it which deals with database deployment in kubernetes.

Also, what they suggest about using ConfigMaps to store db secrets feels wrong.
A quote from [this info about ConfigMaps](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/#understanding-configmaps-and-pods):

> ConfigMap is similar to Secrets, but provides a means of working with strings that don’t contain sensitive information.

This makes it sounds like using [Secrets])(https://kubernetes.io/docs/concepts/configuration/secret/) is a proper way to go.

I successfully created a secret by applying a config file similar to this (see keepass attachment for actual file):

```
apiVersion: v1
kind: Secret
metadata:
  name: server-prisma-secrets
type: Opaque
stringData:
  PRISMA_CONFIG: |
    port: 4466
    # uncomment the next line and provide the env var PRISMA_MANAGEMENT_API_SECRET=my-secret to activate cluster security
    # managementApiSecret: my-secret
    databases:
      default:
        connector: postgres
        host: ******
        database: ******
        user: ******
        password: ******
        ssl: true
        rawAccess: true
        port: '******'
        migrations: true
```

And added deployment and service configs that use this secret:

- k8s/server-prisma-deployment.yml
- k8s/server-prisma-cluster-ip-service.yml

And finally temporarily exposed prisma-server publicly to make sure it comes up and connects to the db properly.
I can see the playground, existing db schema, and can run a query to get all users. Works!

LIGHT BULB MOMENT

- https://medium.com/@shalkam/setting-up-prisma-graphql-on-kubernetes-cc1d70988d7d

I don't need to temporarily expose prisma-server publicaly.

There is a way to do port-forwarding via kubectl:

```
kubectl port-forward svc/server-prisma-cluster-ip-service 5577:4466
```

With the above running I can just point local browser to http://localhost:5577 to access it!
That URL also works for `prisma deploy`, all you need to do is update the port in:
* server-gql/prisma/prisma.yml
   * endpoint: http://localhost:5577
   * prisma deploy
* server-gql/src/db/seed.js
   * endpoint: http://localhost:5577
   * prisma reset -f
   * npm run db:seed
