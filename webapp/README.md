This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Docker

### Development

To build a Docker image for development:

```
sudo docker build -f Dockerfile.dev .
```

And to run the webapp in that Docker image:

```
sudo docker-compose up
```

Note that the above command depends on the configuration specified in the `docker-compose.yml` file located in the project root directory.

After a bit of crunching the Docker image should start serving the app on localhost:3000 and you should also see the tests executed atomatically on any file change.

To see the app in action, point your browser to:

```
http://localhost:3000
```

Hot reloading should work, so if you make a change to any source file and save it, browser should automatically refresh with your change.

#### Testing

An alternative way to run tests if you need more interactive control:

```
sudo docker-compose up
# start a new terminal window
sudo docker ps (to get the id)
sudo docker exec -it <id> npm run test
```

And sometimes it's useful to get into the running Docker images shell:

```
sudo docker-compose up
# start a new terminal window
sudo docker ps (to get the id)
sudo docker exec -it <id> sh
```

### Production

Production image is different from development image.

In the development image sources are compiled on the fly and served from memory by the development server. The development server is great for development, but not suitable for production.

The production Docker image bundles a very efficient nginx server exposing our stitically compiled and minimized app on standard port 80. That image is eventually deployed to a Kubernetes cluster in the cloud.

It is defined by the `Dockerfile` file in the project's root directory and can be build with this command:

```
sudo docker build .
```

The output of the build result should print out an image id that looks something like

```
...
Successfully built fbe79a90a820
```

To run this image locally, copy and paste the above ID into the following command. Note that you can quickly re-build the image to get that ID:

```
sudo docker run -p 8080:80 fbe79a90a820
```

The above maps the development machines port 8080 to Docker image's port 80, and thus we can see the app runing by pointing the browser to:

```
https://localhost:8080
```

When happy with the results, build again tagging it with new version and update ../k8s/webapp-deployment.yml version (for manual deployments until CI/CD is setup)

```
sudo docker build -t thomasmarkiewicz/fostershare-webapp .
sudo docker build -t thomasmarkiewicz/forestshare-webapp:0.0.2 .
sudo docker push thomasmarkiewicz/fostershare-webapp:latest
sudo docker push thomasmarkiewicz/forestshare-webapp:0.0.2
# update ../k8s/webapp-deployment.yml version
kubectl apply -f k8s
```

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
