import { createYield } from 'typescript';

// see: https://docs.cypress.io/guides/getting-started/writing-your-first-test.html#In-action

describe('My First Test', function() {
  it('Does not do much!', function() {
    /*
      Given a user visits https://example.cypress.io
      When they click the link labeled 'Learn React'
      And they type “fake@email.com“ into the .actions-email input
      Then the URL should include /commands/actions
      And the .actions-email input has “fake@email.com“ as its value
    */

    cy.visit('https://fostershare.app');

    cy.contains('Learn React').click();

    // it should stay on the same page (and another window for the link)
    cy.url().should('include', 'fostershare.app');

    // We can use cy.get() to select an element based on a CSS class.
    // we can use the .type() command to enter text into the selected input
    /*
        cy.get('.action-email')
          .type('fake@email.com')
          .should('have.value', 'fake@email.com')
    */

    // You can also pause and debug for debugging your app
    /*
        cy.pause()
        cy.debug()
    */
  });
});
