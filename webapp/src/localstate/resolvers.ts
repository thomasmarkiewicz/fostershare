import gql from 'graphql-tag';

const typeDefs = gql`
	extend type Query {
		isLoggedIn: Boolean!
		cartItems: [ID!]! 
	}

	extend type Launch {
		isInCart: Boolean!
	}

	extend type Mutation {
		addOrRemoveFromCart(id: ID!): [Launch]
	}
`;

const resolvers = {
    /*
    Launch: {
        isInCart: (launch, _, { cache }) => {
          const { cartItems } = cache.readQuery({ query: GET_CART_ITEMS });
          return cartItems.includes(launch.id);
        },
      },
      */
};

export { typeDefs, resolvers };
