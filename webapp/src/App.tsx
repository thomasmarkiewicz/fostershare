import React, { StrictMode, FC } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { history } from './util';
import {
	Home,
	Hubs,
	Hub,
	Landing,
	Login,
	Register,
	Profile,
	Shelter,
} from './screen';
import { GlobalState } from './state';
import { apollo } from './apollo';
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
	palette: {
		primary: {
			light: '#415765',
			main: '#415765',
			dark: '#415765',
			contrastText: '#fff',
		},
		secondary: {
			light: '#5190bd',
			main: '#5190bd',
			dark: '#5190bd',
			contrastText: '#fff',
		},
	},
});

const App: FC = props => {
	return (
		<StrictMode>
			<GlobalState>
				<ApolloProvider client={apollo}>
					<CssBaseline />
					<ThemeProvider theme={theme}>
						<MuiPickersUtilsProvider utils={MomentUtils}>
							<Router history={history}>
								<Switch>
									<Route exact path="/" component={Home} />
									<Route exact path="/landing" component={Landing} />
									<Route exact path="/hubs" component={Hubs} />
									<Route exact path="/hub/:id" component={Hub} />
									<Route path="/login" component={Login} />
									<Route path="/register" component={Register} />
									<Route exact path="/profile" component={Profile} />
									<Route exact path="/shelter/:id" component={Shelter} />
								</Switch>
							</Router>
						</MuiPickersUtilsProvider>
					</ThemeProvider>
				</ApolloProvider>
			</GlobalState>
		</StrictMode>
	);
};

export { App };
