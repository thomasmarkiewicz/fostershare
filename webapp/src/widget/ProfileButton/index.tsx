import React, { FC } from 'react';
import Button from '@material-ui/core/Button';
import { history } from '../../util';
import { useGlobalState } from '../../state';

const ProfileButton: FC = props => {
	const { state } = useGlobalState();

	return state.isLoggedIn ? (
		<Button color="inherit" onClick={() => history.push('/profile')}>
			Profile
		</Button>
	) : (
		<Button color="inherit" onClick={() => history.push('/login')}>
			Login
		</Button>
	);
};

export { ProfileButton };
