import React, { FC } from 'react';
import { TextField } from '@material-ui/core';
import { useField, FieldConfig } from 'formik';

interface FormTextFieldProps extends FieldConfig {
	label: string;
	helperText?: string;
}
const FormTextField: FC<FormTextFieldProps> = ({
	label,
	helperText,
	...props
}) => {
	// useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
	// which we can spread on <input> and also replace ErrorMessage entirely.
	const [field, meta] = useField(props);
	const isError = Boolean(meta.touched && meta.error);
	const fieldText = isError ? meta.error : helperText;
	return (
		<TextField
			variant="filled"
			label={label}
			error={isError}
			helperText={fieldText}
			margin="normal"
			fullWidth
			{...field}
			{...props}
		/>
	);
};

export { FormTextField };
