import React, { useState, createContext, FC, useContext } from 'react';

export interface GlobalStateType {
    token?: string;
    isLoggedIn: boolean;
}

export interface GlobalStateContextType {
    state: GlobalStateType;
    setState: (state: GlobalStateType) => void;
};

const defaultGlobalContext: GlobalStateContextType = {
    state: {
        isLoggedIn: !!localStorage.getItem('token'),
    },
    setState: (state: GlobalStateType) => void {}
}

const GlobalStateContext = createContext<GlobalStateContextType>(defaultGlobalContext)

const GlobalState: FC = ({children}) => {
    const [state, setState] = useState<GlobalStateType>(defaultGlobalContext.state);
    return (
        <GlobalStateContext.Provider value={{state, setState}}>
            {children}
        </GlobalStateContext.Provider>
    )
}

function useGlobalState(): GlobalStateContextType {
    return useContext(GlobalStateContext);
}

export { GlobalState, useGlobalState }