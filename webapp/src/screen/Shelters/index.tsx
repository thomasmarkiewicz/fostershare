import React, { FC } from 'react';
import { useQuery } from '@apollo/react-hooks';
import {
	AppBar,
	Container,
	List,
	ListItem,
	ListItemText,
	Toolbar,
	Typography,
} from '@material-ui/core';
import { useStyles } from '../../style';
import { HideOnScroll } from '../../widget/HideOnScroll';
import { ProfileButton } from '../../widget/ProfileButton';
import { GET_SHELTERS } from '../../queries';
import { history } from '../../util';

interface Location {
	id: string;
	line1: string;
	line2: string;
	line3: string;
	city: string;
	region: string;
	postcode: string;
	country: string;
	web: string;
	phone: string;
	fax: string;
	latitude: number;
	longitude: number;
}

interface Shelter {
	id: string;
	name: string;
	locations: Location[];
}

const Shelters: FC = props => {
	const classes = useStyles();
	const { data, loading, error } = useQuery(GET_SHELTERS);

	//if (loading) return <div>Loading...</div>;
	//if (error) return <p>ERROR</p>;

	return (
		<div className={classes.root}>
			<HideOnScroll {...props}>
				<AppBar>
					<Toolbar>
						<Typography variant="h6" className={classes.title}>
							Shelters
						</Typography>
						<ProfileButton />
					</Toolbar>
				</AppBar>
			</HideOnScroll>
			<Toolbar />

			<Container maxWidth="md">
				<List disablePadding={true}>
					{data &&
						data.shelters &&
						data.shelters.map((shelter: any) => {
							const loc = shelter.locations[0].location;
							const line2 = loc
								? `${loc.line1}, ${loc.city}, ${loc.region} ${loc.postcode}`
								: '';
							return (
								<ListItem
									key={shelter.id}
									disableGutters={true}
									button={true}
									onClick={() => history.push(`/shelter/${shelter.id}`)}
								>
									<ListItemText primary={shelter.name} secondary={line2} />
								</ListItem>
							);
						})}
				</List>
			</Container>
		</div>
	);
};

export { Shelters };
