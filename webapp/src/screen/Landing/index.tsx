import React, { FC } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { Shelters } from '../Shelters';
import { Hubs } from '../Hubs';
import { GET_ME } from '../../queries';
import { useGlobalState } from '../../state';

const Landing: FC = () => {
	const { data, error, loading } = useQuery(GET_ME, {
		onCompleted(d) {
			console.log('GET_ME completed', d);
		},
		onError(e) {
			console.log('GET_ME error', e);
		},
    });
    
	const { state } = useGlobalState();
	console.log('global state', state);

	return state.isLoggedIn && 
		data &&
		data.me.roles.includes('admin') ? (
		<Shelters />
	) : (
		<Hubs />
	);
};

export { Landing };
