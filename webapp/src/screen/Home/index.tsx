import React, { FC } from 'react';
import LogoWithHeadline from '../../assets/logo_with_headline_02.png';
import Background from '../../assets/background_02.png';
import WalkMe from '../../assets/dog_asking_walk.png';
import {
	AppBar,
	Box,
	Button,
	Container,
	Paper,
	Toolbar,
	Typography,
} from '@material-ui/core';

const containerStyle = {
	width: '100%',
};

const logoStyle = {
	backgroundImage: `url(${LogoWithHeadline})`,
	backgroundPosition: 'center',
	backgroundSize: 'cover',
	backgroundRepeat: 'no-repeat',
	//height: '255px',
	//width: '371px',
	height: '120px',
	//width: '100%'
};

const bgStyle = {
	backgroundImage: `url(${Background})`,
	backgroundPosition: 'center',
	backgroundSize: 'cover',
	backgroundRepeat: 'no-repeat',
	height: '1080px',
	width: '100%', //'850px',
};

const imgStyle = {
	margin: 'auto',
	padding: '10px',
};

const Home: FC = () => {
	console.log('Background', LogoWithHeadline);
	return (
		<div style={containerStyle}>
			<div style={bgStyle}>
				<img src={LogoWithHeadline} style={{marginTop: '24px'}} />
                <img src={WalkMe} style={{opacity: '10%', height: '260px', paddingTop: '72px', paddingLeft: '100px'}} />
				<Container>
					<Button
                        style={{marginTop: '12px'}}
                        fullWidth={true}
                        size="large"
						variant="contained"
						color="secondary"
						href="landing"
					>
						Schedule a walk
					</Button>
				</Container>
			</div>
		</div>
	);
};

export { Home };
