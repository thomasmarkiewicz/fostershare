import React, { FC } from 'react';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
	AppBar,
	Box,
	Button,
	Container,
	IconButton,
	Paper,
	Toolbar,
	Typography,
} from '@material-ui/core';
import BackIcon from '@material-ui/icons/ArrowBack';
import { useStyles } from '../../style';
import { history } from '../../util';
import { HideOnScroll } from '../../widget/HideOnScroll';
import { FormTextField } from '../../widget/FormTextField';
import { SnackbarVariant } from '../../widget/SnackbarVariant';
import { useGlobalState } from '../../state';

const REGISTER_USER = gql`
	mutation registerUser(
		$name: String!
		$email: String!
		$passphrase: String!
		$phone: String!
	) {
		registerUser(
			data: { name: $name, email: $email, password: $passphrase, phone: $phone }
		) {
			user {
				id
				name
				email
			}
			token
		}
	}
`;

interface RegistrationFormProps {
	register: (options?: any) => any;
}

const RegisterForm: FC<RegistrationFormProps> = ({ register }) => {
	const classes = useStyles();
	return (
		<Formik
			initialValues={{
				name: '',
				email: '',
				passphrase: '',
				confirm: '',
				phone: '',
			}}
			validationSchema={Yup.object({
				name: Yup.string().required('Required'),
				email: Yup.string()
					.email()
					.required('Required'),
				passphrase: Yup.string().required('Required'),
				confirm: Yup.string()
					.required('Required')
					.oneOf([Yup.ref('passphrase'), null], 'Passphrase must match'),
				phone: Yup.string().required('Required'),
			})}
			onSubmit={(values, { setSubmitting }) => {
				const { name, email, passphrase, phone } = values;
				register({
					variables: {
						name,
						email,
						passphrase,
						phone,
					},
				});
			}}
		>
			<Form translate="false">
				<Paper className={classes.form}>
					<FormTextField name="name" label="Name" />
					<FormTextField name="email" label="Email Address" />
					<FormTextField name="passphrase" label="Passphrase" type="password" />
					<FormTextField
						name="confirm"
						label="Confirm Passphrase"
						type="password"
					/>
					<FormTextField name="phone" label="Phone" />

					<Box mt={3} mb={2}>
						<Button
							variant="contained"
							color="secondary"
							size="large"
							fullWidth
							type="submit"
						>
							Register
						</Button>
					</Box>
				</Paper>
			</Form>
		</Formik>
	);
};

const Register: FC = props => {
	const [open, setOpen] = React.useState(false);
	const classes = useStyles();
	const client = useApolloClient();
	const gState = useGlobalState();
	const [register, { loading, error }] = useMutation(REGISTER_USER, {
		onCompleted({ registerUser: { user, token } }) {
			localStorage.setItem('token', token);
			client.writeData({ data: { isLoggedIn: true, user } });
			gState.setState({ ...gState.state, isLoggedIn: true });
			history.goBack();
			history.goBack();
		},
		onError(e) {
			setOpen(true);
		},
	});

	// if (loading) return <div>Loading...</div>;

	return (
		<div className={classes.root}>
			<HideOnScroll {...props}>
				<AppBar>
					<Toolbar>
						<IconButton
							edge="start"
							className={classes.menuButton}
							color="inherit"
							aria-label="menu"
							onClick={() => history.goBack()}
						>
							<BackIcon />
						</IconButton>
						<Typography variant="h6" className={classes.title}>
							Register
						</Typography>
					</Toolbar>
				</AppBar>
			</HideOnScroll>
			<Toolbar />
			<Container maxWidth="sm">
				<Box mt={4}>
					<RegisterForm register={register} />
					<SnackbarVariant
						open={open}
						onClose={() => setOpen(false)}
						variant="error"
						message="Registration failed"
					/>
				</Box>
			</Container>
		</div>
	);
};

export { Register };
