import React, { FC } from 'react';
import moment from 'moment';
import { useQuery } from '@apollo/react-hooks';
import {
	AppBar,
	Container,
	Toolbar,
	Typography,
	List,
	ListItem,
	ListItemText,
} from '@material-ui/core';
import { useStyles } from '../../style';
import { HideOnScroll } from '../../widget/HideOnScroll';
import { ProfileButton } from '../../widget/ProfileButton';
import { GET_HUB_SUMMARIES } from '../../queries';
import { history } from '../../util';

interface HubSummary {
	id: string;
	name: string;
	open: string;
	close: string;
}

const Hubs: FC = props => {
	const classes = useStyles();

	const { data, loading, error } = useQuery(GET_HUB_SUMMARIES);
	if (loading) return <div>Loading...</div>;
	if (error) return <p>ERROR</p>;

	console.log('hub data', data);

	return (
		<div className={classes.root}>
			<HideOnScroll {...props}>
				<AppBar>
					<Toolbar>
						<Typography variant="h6" className={classes.title}>
							Hubs
						</Typography>
						<ProfileButton />
					</Toolbar>
				</AppBar>
			</HideOnScroll>
			<Toolbar />

			<Container maxWidth="md">
				<List disablePadding={true}>
					{data &&
						data.hubSummaries &&
						data.hubSummaries.map((hub: HubSummary) => {
							const secondary = hub.open ? `${moment(hub.open).format(
								'dddd, MMMM Do',
							)} • ${moment(hub.open).format('h:mm a')} to ${moment(
								hub.close,
							).format('h:mm a')}` : 'Next open date is not scheduled';
							return (
								<ListItem
									key={hub.id}
									disableGutters={true}
									button={true}
									onClick={() => history.push(`/hub/${hub.id}`)}
								>
									<ListItemText
										primary={hub.name}
										secondary={secondary}
									/>
								</ListItem>
							);
						})}
				</List>
			</Container>
		</div>
	);
};

export { Hubs };
