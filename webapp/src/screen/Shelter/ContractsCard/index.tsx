import React, { FC } from 'react';
import { FieldArray } from 'formik';
import { v4 as uuid } from 'uuid';
import {
	Button,
	Card,
	CardContent,
	CardActions,
	Divider,
	List,
	ListItem,
	ListItemText,
	ListItemSecondaryAction,
	Menu,
	MenuItem,
	Typography,
	IconButton,
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useTheme } from '@material-ui/core/styles';
import { useStyles } from '../../../style';
import { ContractDialog } from './ContractDialog';
import { MockContract } from '../../../mock/shelters';
import { MOCK_EMPTY_CONTRACT } from '../../../mock/contracts';

interface ContractsCardProps {
	data: MockContract[];
	onSave: (data: MockContract[]) => void;
}

const ContractsCard: FC<ContractsCardProps> = ({ data = [], onSave }) => {
	const classes = useStyles();
	const theme = useTheme();
	const [contract, setContract] = React.useState(MOCK_EMPTY_CONTRACT);
	const [index, setIndex] = React.useState(0);
	const [open, setOpen] = React.useState(false);
	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

	const openDialog = () => setOpen(true);
	const closeDialog = () => setOpen(false);
	const closeMenu = () => setAnchorEl(null);

	return (
		<FieldArray name="contracts">
			{({ move, swap, push, insert, remove, unshift, pop, replace, form }) => (
				<div>
					<Card
						className={classes.card}
						style={{ marginTop: theme.spacing(3) }}
					>
						<CardContent>
							<Typography
								className={classes.title}
								color="textSecondary"
								gutterBottom
							>
								Contracts
							</Typography>
							<Divider />
							<List dense={true}>
								{data.map((contract, index) => (
									<ListItem
										key={index}
										button={true}
										disableGutters={true}
										onClick={() => window.open(contract.url)}
									>
										<ListItemText primary={contract.name} />
										<ListItemSecondaryAction
											onClick={event => {
												setIndex(index);
												setAnchorEl(event.currentTarget);
											}}
										>
											<IconButton edge="end">
												<MoreVertIcon />
											</IconButton>
										</ListItemSecondaryAction>
									</ListItem>
								))}
							</List>
						</CardContent>
						<CardActions>
							<Button
								size="small"
								color="secondary"
								onClick={() => {
									setContract({ ...MOCK_EMPTY_CONTRACT, id: uuid() });
									openDialog();
								}}
							>
								Add
							</Button>
						</CardActions>
					</Card>

					<ContractDialog
						open={open}
						data={contract}
						onSave={(contract: MockContract) => {
							// add or update?
							const exists = data.find(c => c.id === contract.id);
							if (!exists) {
								push(contract);
							} else {
								data.forEach((c, i) => {
									if (c.id === contract.id) {
										replace(i, contract);
									}
								});
							}
							closeDialog();
						}}
						onCancel={closeDialog}
					/>

					<Menu
						id="edit-delete-menu"
						anchorEl={anchorEl}
						keepMounted
						open={Boolean(anchorEl)}
						onClose={closeMenu}
					>
						<MenuItem onClick={()=>{
							setContract({...MOCK_EMPTY_CONTRACT, ...data[index]});
							closeMenu();
							openDialog();
						}}>Edit</MenuItem>
						<MenuItem
							onClick={() => {
								remove(index);
								closeMenu();
							}}
						>
							Delete
						</MenuItem>
					</Menu>
				</div>
			)}
		</FieldArray>
	);
};

export { ContractsCard };
