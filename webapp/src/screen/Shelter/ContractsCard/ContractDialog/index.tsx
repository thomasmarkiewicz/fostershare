import React, { FC, ReactNode } from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Formik } from 'formik';
import * as Yup from 'yup';
import {
	Button,
	Dialog,
	DialogActions,
	DialogTitle,
	DialogContent,
	DialogContentText,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { FormTextField } from '../../../../widget/FormTextField';
import { MockContract } from '../../../../mock/shelters';

interface ContractDialogProps {
	data: MockContract;
	open: boolean;
	onSave: (contract: MockContract) => void;
	onCancel: () => void;
}

const ContractDialog: FC<ContractDialogProps> = ({
	data,
	open,
	onSave,
	onCancel
}) => {
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

	return (
		<Formik
			initialValues={{ ...data }}
			enableReinitialize
			validationSchema={Yup.object({
				name: Yup.string().required('Required'),
				url: Yup.string()
					.url('Must be a valid URL')
					.required('Required'),
			})}
			onSubmit={onSave}
		>
			{({ isValid, isSubmitting, dirty, submitForm, values }): ReactNode => {
				const saveDisabled = !isValid || !dirty || isSubmitting;
				return (
					<div role="form">
						<Dialog fullScreen={fullScreen} open={open}>
							<DialogTitle id="responsive-dialog-title">
								{'Add a contract'}
							</DialogTitle>
							<DialogContent>
								<DialogContentText>
									Please provide a short name, description, and a link to the
									contract:
								</DialogContentText>
								<FormTextField name="name" label="Name *" />
								<FormTextField name="url" label="Link *" />
								<FormTextField name="description" label="Description" />
							</DialogContent>
							<DialogActions>
								<Button
									onClick={onCancel}
									color="secondary"
								>
									Cancel
								</Button>
								<Button
									onClick={submitForm}
									color="secondary"
									disabled={saveDisabled}
								>
									Save
								</Button>
							</DialogActions>
						</Dialog>
					</div>
				);
			}}
		</Formik>
	);
};

export { ContractDialog };
