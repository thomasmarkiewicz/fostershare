import React, { FC } from 'react';
import {
	Card,
	CardContent,
	Divider,
	Typography,
} from '@material-ui/core';
import { useStyles } from '../../../style';
import { FormTextField } from '../../../widget/FormTextField';

const InfoCard: FC = props => {
	const classes = useStyles();
	return (
		<Card className={classes.card}>
			<CardContent>
				<Typography
					className={classes.title}
					color="textSecondary"
					gutterBottom
				>
					Info
				</Typography>
				<Divider />
				<FormTextField name="name" label="Shelter Name*" />
				<FormTextField name="line1" label="Address Line 1*" />
				<FormTextField name="line2" label="Address Line 2" />
				<FormTextField name="city" label="City*" />
				<FormTextField name="region" label="State*" />
				<FormTextField name="postcode" label="Zip*" />
				<FormTextField name="web" label="Web URL" />
				<FormTextField name="phone" label="Phone" />
				<FormTextField name="fax" label="Fax" />
			</CardContent>
		</Card>
	);
};

export { InfoCard };
