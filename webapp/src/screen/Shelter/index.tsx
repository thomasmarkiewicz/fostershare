import React, { FC, ReactNode } from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
	AppBar,
	Button,
	Container,
	Grid,
	IconButton,
	Toolbar,
	Typography,
} from '@material-ui/core';
import BackIcon from '@material-ui/icons/ArrowBack';
import { useTheme } from '@material-ui/core/styles';
import { useStyles } from '../../style';
import { HideOnScroll } from '../../widget/HideOnScroll';
import { ProfileButton } from '../../widget/ProfileButton';
import { history } from '../../util';
import { DogsCard } from './DogsCard';
import { HubsCard } from './HubsCard';
import { ContractsCard } from './ContractsCard';
import { InfoCard } from './InfoCard';
import { MOCK_EMPTY_SHELTER, MockShelter } from '../../mock/shelters';

const Shelter: FC = props => {
	const classes = useStyles();
	const theme = useTheme();
	const [shelter, setShelter] = React.useState(MOCK_EMPTY_SHELTER);

	const onShelterChange = (
		event: React.ChangeEvent<{}>,
		newShelter: MockShelter,
	) => {
		setShelter(newShelter);
	};

	const bull = <span className={classes.bullet}>•</span>;

	return (
		<div className={classes.root}>
			<HideOnScroll {...props}>
				<AppBar>
					<Toolbar>
						<IconButton
							edge="start"
							className={classes.menuButton}
							color="inherit"
							aria-label="menu"
							onClick={() => history.goBack()}
						>
							<BackIcon />
						</IconButton>
						<Typography variant="h6" className={classes.title}>
							Shelter
						</Typography>
						<ProfileButton />
					</Toolbar>
				</AppBar>
			</HideOnScroll>

			<Toolbar />

			<Formik
				initialValues={shelter}
				onSubmit={values => {
					// TODO: implement
					console.log('onSubmit', values);
				}}
			>
				{({ values, isValid, isSubmitting, submitForm }): ReactNode => {
					const submitDisabled = !isValid || isSubmitting;
					return (
						<Form translate="false">
							<Container
								maxWidth="lg"
								style={{
									marginTop: theme.spacing(3),
									marginBottom: theme.spacing(3),
								}}
							>
								<Grid container spacing={3}>
									<Grid item xs={12} sm={6}>
										<InfoCard />
									</Grid>
									<Grid item xs={12} sm={6}>
										<HubsCard
											data={values.hubs}
											onSave={data => setShelter({ ...shelter, hubs: data })}
										/>
										<DogsCard
											data={values.dogs}
											onSave={data => setShelter({ ...shelter, dogs: data })}
										/>
										<ContractsCard
											data={values.contracts}
											onSave={data =>
												setShelter({ ...shelter, contracts: data })
											}
										/>
									</Grid>
								</Grid>
								<Button
									variant="contained"
									color="secondary"
									size="large"
									fullWidth
									disabled={submitDisabled}
									type="submit"
									style={{ marginTop: 24 }}
								>
									Save
								</Button>
							</Container>
						</Form>
					);
				}}
			</Formik>
		</div>
	);
};

export { Shelter };
