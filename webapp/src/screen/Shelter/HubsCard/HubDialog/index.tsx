import React, { FC, ReactNode } from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Formik } from 'formik';
import * as Yup from 'yup';
import {
	Button,
	Dialog,
	DialogActions,
	DialogTitle,
	DialogContent,
	DialogContentText,
	Grid,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { FormTextField } from '../../../../widget/FormTextField';
import { MockHub } from '../../../../mock/hubs';

interface HubDialogProps {
	data: MockHub;
	open: boolean;
	onSave: (contract: MockHub) => void;
	onCancel: () => void;
}

const HubDialog: FC<HubDialogProps> = ({ data, open, onSave, onCancel }) => {
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

	return (
		<Formik
			initialValues={{ ...data }}
			enableReinitialize
			validationSchema={Yup.object({
				name: Yup.string().required('Required'),
				location: Yup.object({
					line1: Yup.string().required('Required'),
					city: Yup.string().required('Required'),
					region: Yup.string().required('Required'),
					postcode: Yup.string().required('Required'),
				}),
			})}
			onSubmit={onSave}
		>
			{({ isValid, isSubmitting, dirty, submitForm, values }): ReactNode => {
				const saveDisabled = !isValid || !dirty || isSubmitting;
				return (
					<div role="form">
						<Dialog fullScreen={fullScreen}  open={open}>
							<DialogTitle id="hubs-dialog-title">{'Add a hub'}</DialogTitle>
							<DialogContent style={{height: 500}}>
								<DialogContentText>
									Please provide all required hub details:
								</DialogContentText>
								<FormTextField name="name" label="Name *" />
								<FormTextField name="location.line1" label="Address Line 1*" />
								<Grid container spacing={3}>
									<Grid item xs={6}>
										<FormTextField name="location.city" label="City*" />
									</Grid>
									<Grid item xs={6}>
										<FormTextField name="location.region" label="State*" />
									</Grid>
								</Grid>
								<Grid container spacing={3}>
									<Grid item xs={6}>
										<FormTextField name="location.postcode" label="Zip*" />
									</Grid>
									<Grid item xs={6}>
										<FormTextField name="location.phone" label="Phone" />
									</Grid>
								</Grid>
								<Grid container spacing={3}>
									<Grid item xs={6}>
									<FormTextField name="location.latitude" label="Latitude" />
									</Grid>
									<Grid item xs={6}>
									<FormTextField name="location.longitude" label="Longitude" />
									</Grid>
								</Grid>
								
								
							</DialogContent>
							<DialogActions>
								<Button onClick={onCancel} color="secondary">
									Cancel
								</Button>
								<Button
									onClick={submitForm}
									color="secondary"
									disabled={saveDisabled}
								>
									Save
								</Button>
							</DialogActions>
						</Dialog>
					</div>
				);
			}}
		</Formik>
	);
};

export { HubDialog };
