import React, { FC } from 'react';
import { FieldArray } from 'formik';
import { v4 as uuid } from 'uuid';
import {
	Button,
	Card,
	CardContent,
	CardActions,
	Divider,
	List,
	ListItem,
	ListItemText,
	ListItemSecondaryAction,
	Menu,
	MenuItem,
	Typography,
	IconButton,
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useTheme } from '@material-ui/core/styles';
import { useStyles } from '../../../style';
import { HubDialog } from './HubDialog';
import { MockHub, MOCK_EMPTY_HUB } from '../../../mock/hubs';

interface HubsCardProps {
	data: MockHub[];
	onSave: (data: MockHub[]) => void;
}

const HubsCard: FC<HubsCardProps> = ({ data = [], onSave }) => {
	const classes = useStyles();
	const theme = useTheme();
	const [hub, setHub] = React.useState(MOCK_EMPTY_HUB);
	const [index, setIndex] = React.useState(0);
	const [open, setOpen] = React.useState(false);
	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

	const openDialog = () => setOpen(true);
	const closeDialog = () => setOpen(false);
	const closeMenu = () => setAnchorEl(null);

	return (
		<FieldArray name="hubs">
			{({ move, swap, push, insert, remove, unshift, pop, replace, form }) => (
				<div>
					<Card className={classes.card}>
						<CardContent>
							<Typography
								className={classes.title}
								color="textSecondary"
								gutterBottom
							>
								Hubs
							</Typography>
							<Divider />
							<List dense={true}>
								{data.map((hub, index) => (
									<ListItem
										key={index}
										button={true}
										disableGutters={true}
										onClick={() =>
											hub.location &&
											hub.location.latitude &&
											hub.location.longitude &&
											window.open(
												`https://www.google.com/maps/@${hub.location.latitude},${hub.location.longitude},17z`,
											)
										}
									>
										<ListItemText primary={hub.name} />
										<ListItemSecondaryAction
											onClick={event => {
												setIndex(index);
												setAnchorEl(event.currentTarget);
											}}
										>
											<IconButton edge="end">
												<MoreVertIcon />
											</IconButton>
										</ListItemSecondaryAction>
									</ListItem>
								))}
							</List>
						</CardContent>
						<CardActions>
							<Button
								size="small"
								color="secondary"
								onClick={() => {
									setHub({ ...MOCK_EMPTY_HUB, id: uuid() });
									openDialog();
								}}
							>
								Add
							</Button>
						</CardActions>
					</Card>

					<HubDialog
						open={open}
						data={hub}
						onSave={(hub: MockHub) => {
							// add or update?
							const exists = data.find(c => c.id === hub.id);
							if (!exists) {
								push(hub);
							} else {
								data.forEach((c, i) => {
									if (c.id === hub.id) {
										replace(i, hub);
									}
								});
							}
							closeDialog();
						}}
						onCancel={closeDialog}
					/>

					<Menu
						id="edit-delete-menu"
						anchorEl={anchorEl}
						keepMounted
						open={Boolean(anchorEl)}
						onClose={closeMenu}
					>
						<MenuItem
							onClick={() => {
								setHub({ ...MOCK_EMPTY_HUB, ...data[index] });
								closeMenu();
								openDialog();
							}}
						>
							Edit
						</MenuItem>
						<MenuItem
							onClick={() => {
								remove(index);
								closeMenu();
							}}
						>
							Delete
						</MenuItem>
					</Menu>
				</div>
			)}
		</FieldArray>
	);
};

export { HubsCard };
