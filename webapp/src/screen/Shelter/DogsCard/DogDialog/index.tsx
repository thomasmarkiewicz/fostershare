import React, { FC, ReactNode } from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Formik } from 'formik';
import * as Yup from 'yup';
import {
	Button,
	Dialog,
	DialogActions,
	DialogTitle,
	DialogContent,
	DialogContentText,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { FormTextField } from '../../../../widget/FormTextField';
import { MockDog } from '../../../../mock/dogs';

interface DogDialogProps {
	data: MockDog;
	open: boolean;
	onSave: (contract: MockDog) => void;
	onCancel: () => void;
}

const DogDialog: FC<DogDialogProps> = ({
	data,
	open,
	onSave,
	onCancel
}) => {
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

	return (
		<Formik
			initialValues={data}
			enableReinitialize
			validationSchema={Yup.object({
				name: Yup.string().required('Required'),
				description: Yup.string(),
				avatarUrl: Yup.string()
					.url('Must be a valid URL'),
			})}
			onSubmit={onSave}
		>
			{({ isValid, isSubmitting, dirty, submitForm, values }): ReactNode => {
				const saveDisabled = !isValid || !dirty || isSubmitting;
				return (
					<div role="form">
						<Dialog fullScreen={fullScreen} open={open}>
							<DialogTitle id="responsive-dialog-title-dog">
								{'Add a dog'}
							</DialogTitle>
							<DialogContent>
								<DialogContentText>
									Please provide a short name, description, and a link to the
									dog's avatar:
								</DialogContentText>
								<FormTextField name="name" label="Name *" />
								<FormTextField name="avatarUrl" label="Avatar Link" />
								<FormTextField name="description" label="Description" />
								<FormTextField name="breed" label="Breed" />
								<FormTextField name="sex" label="Sex" />
								<FormTextField name="age" label="Age" />
								<FormTextField name="code" label="Code" />
								<FormTextField name="homeUrl" label="Web link" />
							</DialogContent>
							<DialogActions>
								<Button
									onClick={onCancel}
									color="secondary"
								>
									Cancel
								</Button>
								<Button
									onClick={submitForm}
									color="secondary"
									disabled={saveDisabled}
								>
									Save
								</Button>
							</DialogActions>
						</Dialog>
					</div>
				);
			}}
		</Formik>
	);
};

export { DogDialog };
