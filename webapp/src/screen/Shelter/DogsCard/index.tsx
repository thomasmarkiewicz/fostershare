import React, { FC } from 'react';
import { FieldArray } from 'formik';
import { v4 as uuid } from 'uuid';
import {
	Button,
	Card,
	CardContent,
	CardActions,
	Divider,
	List,
	ListItem,
	ListItemText,
	ListItemSecondaryAction,
	Menu,
	MenuItem,
	Typography,
	IconButton,
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useTheme } from '@material-ui/core/styles';
import { useStyles } from '../../../style';
import { DogDialog } from './DogDialog';
import { MockDog, MOCK_EMPTY_DOG } from '../../../mock/dogs';

interface DogsCardProps {
	data: MockDog[];
	onSave: (data: MockDog[]) => void;
}

const DogsCard: FC<DogsCardProps> = ({ data = [], onSave }) => {
	const classes = useStyles();
	const theme = useTheme();
	const [dog, setDog] = React.useState(MOCK_EMPTY_DOG);
	const [index, setIndex] = React.useState(0);
	const [open, setOpen] = React.useState(false);
	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

	const openDialog = () => setOpen(true);
	const closeDialog = () => setOpen(false);
	const closeMenu = () => setAnchorEl(null);

	return (
		<FieldArray name="dogs">
			{({ move, swap, push, insert, remove, unshift, pop, replace, form }) => (
				<div>
					<Card
						className={classes.card}
						style={{ marginTop: theme.spacing(3) }}
					>
						<CardContent>
							<Typography
								className={classes.title}
								color="textSecondary"
								gutterBottom
							>
								Dogs
							</Typography>
							<Divider />
							<List dense={true}>
								{data.map((dog, index) => (
									<ListItem
										key={index}
										button={true}
										disableGutters={true}
										onClick={() => dog.homeUrl && window.open(dog.homeUrl)}
									>
										<ListItemText primary={dog.name} />
										<ListItemSecondaryAction
											onClick={event => {
												setIndex(index);
												setAnchorEl(event.currentTarget);
											}}
										>
											<IconButton edge="end">
												<MoreVertIcon />
											</IconButton>
										</ListItemSecondaryAction>
									</ListItem>
								))}
							</List>
						</CardContent>
						<CardActions>
							<Button
								size="small"
								color="secondary"
								onClick={() => {
									setDog({ ...MOCK_EMPTY_DOG, id: uuid() });
									openDialog();
								}}
							>
								Add
							</Button>
						</CardActions>
					</Card>

					<DogDialog
						open={open}
						data={dog}
						onSave={(dog: MockDog) => {
							// add or update?
							const exists = data.find(d => d.id === dog.id);
							if (!exists) {
								push(dog);
							} else {
								data.forEach((d, i) => {
									if (d.id === dog.id) {
										replace(i, dog);
									}
								});
							}
							closeDialog();
						}}
						onCancel={closeDialog}
					/>

					<Menu
						id="edit-delete-dog-menu"
						anchorEl={anchorEl}
						keepMounted
						open={Boolean(anchorEl)}
						onClose={closeMenu}
					>
						<MenuItem
							onClick={() => {
								setDog({ ...MOCK_EMPTY_DOG, ...data[index] });
								closeMenu();
								openDialog();
							}}
						>
							Edit
						</MenuItem>
						<MenuItem
							onClick={() => {
								remove(index);
								closeMenu();
							}}
						>
							Delete
						</MenuItem>
					</Menu>
				</div>
			)}
		</FieldArray>
	);
};

export { DogsCard };
