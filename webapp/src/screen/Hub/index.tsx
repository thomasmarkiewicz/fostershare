import React, { FC, useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import moment from 'moment';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {
	AppBar,
	Button,
	Container,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	IconButton,
	FormControl,
	InputLabel,
	List,
	ListItem,
	ListItemText,
	MenuItem,
	Toolbar,
	Select,
	Typography,
} from '@material-ui/core';
import BackIcon from '@material-ui/icons/ArrowBack';
import { useStyles } from '../../style';
import { HideOnScroll } from '../../widget/HideOnScroll';
import { ProfileButton } from '../../widget/ProfileButton';
import { MOCK_HUBS } from '../../mock';
import { MockHub, MOCK_EMPTY_HUB } from '../../mock/hubs';
import { history } from '../../util';
import { RouteComponentProps } from 'react-router-dom';
import {
	Map,
	GoogleApiWrapper,
	ProvidedProps,
	Marker,
} from 'google-maps-react';
import { GET_HUB_DETAILS } from '../../queries';

function getStyles(name: any, personName: any, theme: any) {
	return {
		fontWeight:
			personName.indexOf(name) === -1
				? theme.typography.fontWeightRegular
				: theme.typography.fontWeightMedium,
	};
}

interface HubType {
	id: string;
	description: string;
}

const mapStyles = {
	width: '100%',
	height: 220,
};

const names = [
	'9:00 AM',
	'9:30 AM',
	'10:00 AM',
	'10:30 AM',
	'11:00 AM',
	'11:30 AM',
	'12:00 PM',
	'12:30 PM',
	'1:00 PM',
	'1:30 PM',
	'2:00 PM',
	'2:30 PM',
	'3:00 PM',
	'3:30 PM',
	'4:00 PM',
	'4:30 PM',
	'5:00 PM',
	'5:30 PM',
	'6:00 PM',
	'6:30 PM',
];

interface MatchParams {
	id: string;
}

interface HubProps extends RouteComponentProps<MatchParams>, ProvidedProps {}

const initialPersonArray: unknown = [];

const HubBase: FC<HubProps> = props => {
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);
	const [walk, setWalk] = React.useState(new Date());
	const [personName, setPersonName] = React.useState(initialPersonArray);
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
	const [selectedDate, setSelectedDate] = React.useState(
		new Date('2014-08-18T21:11:54'),
	);
	const { match } = props;

	const { data, loading, error } = useQuery(GET_HUB_DETAILS, {
		variables: { hubId: match.params['id'] },
	});
	if (loading) return <div>Loading...</div>;
	if (error) return <p>ERROR</p>;

	const { hub } = data;
	console.log('hub details', hub);

	const handleDateChange = (date: Date) => {
		setSelectedDate(date);
	};

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const address = hub.location
		? `${hub.location.line1}, ${hub.location.city}, ${hub.location.region} ${hub.location.postcode}`
		: '';

	const lat =
		hub.location && hub.location.latitude ? hub.location.latitude : 47.444;
	const lng =
		hub.location && hub.location.longitude ? hub.location.longitude : -122.176;

	return (
		<div className={classes.root}>
			<HideOnScroll {...props}>
				<AppBar>
					<Toolbar>
						<IconButton
							edge="start"
							className={classes.menuButton}
							color="inherit"
							aria-label="menu"
							onClick={() => history.goBack()}
						>
							<BackIcon />
						</IconButton>
						<Typography variant="h6" className={classes.title}>
							Hub Details
						</Typography>
						<ProfileButton />
					</Toolbar>
				</AppBar>
			</HideOnScroll>
			<Toolbar />

			<Map
				style={{ width: '100%', height: 220 }}
				google={props.google}
				zoom={11}
				initialCenter={{ lat, lng }}
			>
				<Marker position={{ lat, lng }} />
			</Map>

			<Container maxWidth="lg">
				<List disablePadding={true} style={{ marginTop: '240px' }}>
					<ListItem
						key={hub.id}
						disableGutters={true}
						onClick={() => history.push(`/hub/${hub.id}`)}
					>
						<ListItemText primary={hub.name} secondary={address} />
					</ListItem>
					<ListItem disableGutters={true}>
						<FormControl variant="filled" className={classes.formControl}>
							<InputLabel id="demo-simple-select-filled-label">
								When would you like to walk?
							</InputLabel>
							<Select
								labelId="demo-simple-select-filled-label"
								id="demo-simple-select"
								value={hub.schedule[0].id}
								onChange={() => console.log('changing value')}
							>
								{hub.schedule.map((s: any) => (
									<MenuItem value={s.id}>
										{moment(s.open).format('dddd, MMMM Do')}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</ListItem>
					<ListItem disableGutters={true}>
						<FormControl variant="filled" className={classes.formControl}>
							<InputLabel id="demo-simple-select-filled-label2">
								Select a schedule
							</InputLabel>
							<Select
								labelId="demo-simple-select-filled-label2"
								id="demo-simple-select"
								value={10}
								onChange={() => console.log('changing value')}
							>
								<MenuItem value={10}>Dog #1</MenuItem>
							</Select>
						</FormControl>
					</ListItem>
					<ListItem disableGutters={true}>
						<FormControl variant="filled" className={classes.formControl}>
							<InputLabel id="demo-simple-select-filled-label4">
								Timeslots
							</InputLabel>
							<Select
								labelId="demo-simple-select-filled-label4"
								id="demo-simple-select"
								multiple
								value={personName}
								onChange={e => setPersonName(e.target.value)}
							>
								{names.map(name => (
									<MenuItem
										key={name}
										value={name}
										style={getStyles(name, personName, theme)}
									>
										{name}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</ListItem>
					<ListItem disableGutters={true}>
						<Button
							style={{ marginTop: 8 }}
							variant="contained"
							color="secondary"
							size="large"
							fullWidth
							type="button"
							onClick={() => {
								handleClickOpen();
							}}
						>
							Schedule a walk
						</Button>
					</ListItem>
				</List>
			</Container>

			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogTitle id="alert-dialog-title">{'Walk scheduled!'}</DialogTitle>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">
						<p>
							Thank you for scheduling a walk with {hub.name} on{' '}
							{moment(walk).format('dddd, MMMM Do')}.
						</p>
						<p>
							If you change your mind simply come back and de-select the
							timeslots you requested.
						</p>
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color="primary" autoFocus>
						Dismiss
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
};

const Hub = GoogleApiWrapper({
	apiKey: 'AIzaSyCll4Oif6q96pK0cYmvotdwNH3bDZy5lGI',
})(HubBase);

export { Hub };
