import React, { FC } from 'react';
import { useApolloClient } from '@apollo/react-hooks';
import {
	AppBar,
	Button,
	List,
	ListItem,
	Toolbar,
	Typography,
} from '@material-ui/core';
import { useStyles } from '../../style';
import { history } from '../../util';
import { HideOnScroll } from '../../widget/HideOnScroll';
import { useGlobalState } from '../../state';

const Profile: FC = props => {
	const classes = useStyles();
	const client = useApolloClient();
	const gState = useGlobalState();

	return (
		<div className={classes.root}>
			<HideOnScroll {...props}>
				<AppBar>
					<Toolbar>
						<Typography variant="h6" className={classes.title}>
							Profile
						</Typography>
						<Button
							color="inherit"
							onClick={() => {
								//history.push('/profile/edit');
								history.goBack();
							}}
						>
							Back
						</Button>
					</Toolbar>
				</AppBar>
			</HideOnScroll>
			<Toolbar />

			<List>
				<ListItem key={1}>
					<Button
						size="large"
						fullWidth
						onClick={() => {
							localStorage.removeItem('token');
							client.writeData({ data: { isLoggedIn: false, user: null } });
							gState.setState({ ...gState.state, isLoggedIn: false });
							history.goBack();
						}}
					>
						Log out
					</Button>
				</ListItem>
			</List>
		</div>
	);
};

export { Profile };
