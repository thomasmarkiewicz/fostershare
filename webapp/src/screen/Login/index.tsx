import React, { FC } from 'react';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
	AppBar,
	Box,
	Button,
	Container,
	Paper,
	Toolbar,
	Typography,
} from '@material-ui/core';
import { useStyles } from '../../style';
import { history } from '../../util';
import { HideOnScroll } from '../../widget/HideOnScroll';
import { FormTextField } from '../../widget/FormTextField';
import { SnackbarVariant } from '../../widget/SnackbarVariant';
import { useGlobalState } from '../../state';

const LOGIN_USER = gql`
	mutation loginUser($email: String!, $passphrase: String!) {
		loginUser(data: { email: $email, password: $passphrase }) {
			user {
				id
				name
				email
				roles
			}
			token
		}
	}
`;

interface LoginFormProps {
	login: (options?: any) => any;
}

const LoginForm: FC<LoginFormProps> = ({ login }) => {
	const classes = useStyles();
	return (
		<Formik
			initialValues={{ email: '', passphrase: '' }}
			validationSchema={Yup.object({
				email: Yup.string()
					.email()
					.required('required'),
				passphrase: Yup.string().required('required'),
			})}
			onSubmit={(values, { setSubmitting }) => {
				login({
					variables: { email: values.email, passphrase: values.passphrase },
				});
			}}
		>
			<Form translate="false">
				<Paper className={classes.form}>
					<FormTextField name="email" label="Email Address" />
					<FormTextField name="passphrase" label="Passphrase" type="password" />

					<Box mt={3} mb={2}>
						<Button
							variant="contained"
							color="secondary"
							size="large"
							fullWidth
							type="submit"
						>
							Login
						</Button>
					</Box>

					<Button
						color="secondary"
						size="large"
						fullWidth
						onClick={() => history.push('/register')}
					>
						Register
					</Button>
				</Paper>
			</Form>
		</Formik>
	);
};

const Login: FC = props => {
	const [open, setOpen] = React.useState(false);
	const classes = useStyles();
	const client = useApolloClient();
	const gState = useGlobalState();
	const [login, { loading, error }] = useMutation(LOGIN_USER, {
		onCompleted({ loginUser: { user, token } }) {
			localStorage.setItem('token', token);
			client.writeData({ data: { isLoggedIn: true, user } });
			gState.setState({ ...gState.state, isLoggedIn: true });
			history.goBack();
		},
		onError(e) {
			setOpen(true);
		},
	});

	// if (loading) return <div>Loading...</div>;

	return (
		<div className={classes.root}>
			<HideOnScroll {...props}>
				<AppBar>
					<Toolbar>
						<Typography variant="h6" className={classes.title}>
							Login
						</Typography>
						<Button color="inherit" onClick={() => history.goBack()}>
							Cancel
						</Button>
					</Toolbar>
				</AppBar>
			</HideOnScroll>
			<Toolbar />
			<Container maxWidth="sm">
				<Box mt={4}>
					<LoginForm login={login} />
					<SnackbarVariant
						open={open}
						onClose={() => setOpen(false)}
						variant="error"
						message="Login failed"
					/>
				</Box>
			</Container>
		</div>
	);
};

export { Login };
