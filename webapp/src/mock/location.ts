import uuid from "uuid"

export interface MockLocation {
	id: string;
	line1: string;
	line2?: string;
	line3?: string;
	city: string;
	region: string;
	postcode: string;
	country: string;
	web?: string;
	phone?: string;
	fax?: string;
	latitude?: number;
	longitude?: number;
}

export const MOCK_EMPTY_LOCATION: MockLocation = {
        id: uuid(),
        line1: '',
        city: '',
        region: '',
        postcode: '',
        country: '',
        web: undefined,
        phone: undefined,
        fax: undefined,
        latitude: undefined,
        longitude: undefined,
}

export const MOCK_LOCATIONS: MockLocation[] = [
    {
        id: '1',
        line1: '3721 W 111th St',
        city: 'Chicago',
        region: 'IL',
        postcode: '60655',
        country: 'US',
        web: '',
        phone: '',
        fax: '',
        latitude: 41.6895624,
        longitude: -87.71584,
    },
    {
        id: '2',
        line1: '9200 S Millard Ave',
        city: 'Evergreen Park',
        region: 'IL',
        postcode: '60805',
        country: 'US',
        web: '',
        phone: '',
        fax: '',
        latitude: 41.7198451,
        longitude: -87.7169521,
    },
    {
        id: '3',
        line1: 'W 87th St & S Western Ave',
        city: 'Chicago',
        region: 'IL',
        postcode: '60620',
        country: 'US',
        web: '',
        phone: '',
        fax: '',
        latitude: 41.7221306,
        longitude: -87.7108789,
    },
    {
        id: '4',
        line1: '8445 S Kolin Ave',
        city: 'Chicago',
        region: 'IL',
        postcode: '60652',
        country: 'US',
        web: '',
        phone: '',
        fax: '',
        latitude: 41.7379702,
        longitude: -87.7346715,
    },
    {
        id: '5',
        line1: '3939 W 79th St',
        city: 'Chicago',
        region: 'IL',
        postcode: '60652',
        country: 'US',
        web: '',
        phone: '',
        fax: '',
        latitude: 41.7465463,
        longitude: -87.7272375,
    },
    {
        id: '6',
        line1: '3344 W 71st St',
        city: 'Chicago',
        region: 'IL',
        postcode: '60629',
        country: 'US',
        web: '',
        phone: '',
        fax: '',
        latitude: 41.7650541,
        longitude: -87.7113962,
    },
]