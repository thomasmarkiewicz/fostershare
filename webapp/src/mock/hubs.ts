import { v4 as uuid } from 'uuid';
import { MockLocation, MOCK_LOCATIONS, MOCK_EMPTY_LOCATION } from './location';
import { MockDog, MOCK_DOGS, MOCK_EMPTY_DOG } from './dogs';

export interface MockWalk {
	id: string;
	scheduleId: string;
	user: string;
	start: Date;
	stop: Date;
}

export interface MockWalkSchedule {
	id: string;
	dogId?: string;
	walks: MockWalk[];
}

export interface MockHubDay {
	id: string;
	notes?: string;
	open: Date;
	close: Date;
	schedule: MockWalkSchedule[];
}

export interface MockHub {
	id: string;
	name: string;
	location?: MockLocation;
	dogs: MockDog[];
	schedule: MockHubDay[];
}

export const MOCK_EMPTY_HUB: MockHub = {
	id: uuid(),
	name: '',
	location: MOCK_EMPTY_LOCATION,
	dogs: [],
	schedule: [],
}

export const MOCK_HUBS: MockHub[] = [
	{
		id: uuid(),
		name: 'Mt. Greenwood Park Hub',
		location: MOCK_LOCATIONS[0],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},
	{
		id: uuid(),
		name: 'Duffy Park Hub',
		location: MOCK_LOCATIONS[1],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdfs77sdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-13T09:00:00'),
				close: new Date('2020-06-13T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdfs77sdf098',
								user: 'userid',
								start: new Date('2020-06-13T09:00:00'),
								stop: new Date('2020-06-13T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-20T09:00:00'),
				close: new Date('2020-06-20T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-27T09:00:00'),
				close: new Date('2020-06-27T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},
	{
		id: uuid(),
		name: 'Dan Ryan Woods Pavilion Hub',
		location: MOCK_LOCATIONS[2],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: 'lkjasd123asf2342lkj',
								scheduleId: 'llkfjsdfs77sdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},

	{
		id: uuid(),
		name: 'Durkin Park Hub',
		location: MOCK_LOCATIONS[3],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},
	{
		id: uuid(),
		name: 'Bogan Park Hub',
		location: MOCK_LOCATIONS[4],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdfs77sdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},
	{
		id: uuid(),
		name: 'Tarkington Park Hub',
		location: MOCK_LOCATIONS[5],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdfs77sdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id:uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},

	{
		id: uuid(),
		name: 'Mt. Greenwood Park Hub',
		location: MOCK_LOCATIONS[0],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},
	{
		id: uuid(),
		name: 'Duffy Park Hub',
		location: MOCK_LOCATIONS[1],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdfs77sdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},
	{
		id: uuid(),
		name: 'Dan Ryan Woods Pavilion Hub',
		location: MOCK_LOCATIONS[2],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdfs77sdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},

	{
		id: uuid(),
		name: 'Durkin Park Hub',
		location: MOCK_LOCATIONS[3],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},
	{
		id: uuid(),
		name: 'Bogan Park Hub',
		location: MOCK_LOCATIONS[4],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdfs77sdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},
	{
		id: uuid(),
		name: 'Tarkington Park Hub',
		location: MOCK_LOCATIONS[5],
		dogs: MOCK_DOGS,
		schedule: [
			{
				id: uuid(),
				open: new Date('2020-05-23T09:00:00'),
				close: new Date('2020-05-23T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [
							{
								id: uuid(),
								scheduleId: 'llkfjsdfs77sdf098',
								user: 'userid',
								start: new Date('2020-05-23T09:00:00'),
								stop: new Date('2020-05-23T10:00:00'),
							},
						],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-05-30T09:00:00'),
				close: new Date('2020-05-30T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
			{
				id: uuid(),
				open: new Date('2020-06-06T09:00:00'),
				close: new Date('2020-06-06T18:00:00'),
				schedule: [
					{
						id: uuid(),
						walks: [],
					},
					{
						id: uuid(),
						walks: [],
					},
				],
			},
		],
	},
];
