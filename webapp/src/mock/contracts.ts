import { v4 as uuid } from 'uuid';
import { MockLocation, MOCK_EMPTY_LOCATION } from './location';
import { MockDog, MOCK_EMPTY_DOG } from './dogs';
import { MockHub } from './hubs';

export interface MockContract {
    id: string;
    name: string;
    description?: string;
    url: string;
}

export const MOCK_EMPTY_CONTRACT: MockContract = {
    id: uuid(),
    name: '',
    description: '',
    url: ''
}

