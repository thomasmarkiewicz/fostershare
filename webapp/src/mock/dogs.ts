import uuid from "uuid"

export interface MockDog {
    id: string;
    code?: string; 
	name: string;
    description?: string;
    notes?: string;
    sex?: string;
    homeUrl?: string; 
    avatarUrl?: string;
    photoUrls?: string[];
    breed?: string[];
    age?: number;
    lbs?: number;
}

export const MOCK_EMPTY_DOG: MockDog =
{
    id: uuid(),
    code: "",
    name: "",
    description: "",
    notes: "",
    sex: "",
    homeUrl: "",
    avatarUrl: "",
    photoUrls: [],
    age: undefined,
    breed: []
}

export const MOCK_DOGS: MockDog[] = [
    {
        id: "1",
        code: "SSHS-A-3146",
        name: "Koko",
        description: `
            Koko is a beautiful, tall dog, looking for an active forever family to keep her busy. 
            Her ideal home would have an experienced dog owner - especially if the owner had experience with Akitas. 
            Koko enjoys being outside and chasing after her toys. 
            Koko loves plush toys and squeaky toys.
        `,
        notes: `Adult-only home preferred`,
        sex: "female",
        homeUrl: 'https://www.shelterluv.com/publish_animal/SSHS-A-3146?embedded=1&columns=2&iframeId=shelterluv_embed_2661561401138700&saved_query=3476#https://www-southsuburbanhumane-org.filesusr.com/html/a5d6fe_216e31003955bb7662a2d78c9461a8bf.html',
        avatarUrl: "https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/07/18/16/20190718161718.png?itok=EQ-NeLWB",
        photoUrls: [
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/03/19/13/20190319130334_0.png?itok=pE6KvT5Q',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/03/19/13/20190319130436.png?itok=PaeUftL8',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/05/01/12/20190501121817.png?itok=6_cBWyj4',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/05/14/16/20190514163251.png?itok=2tgeG9mu',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/05/14/16/20190514163305.png?itok=EQNwB_QO',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/05/14/16/20190514163328.png?itok=tCtMl0Xn',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/05/14/16/20190514163328.png?itok=tCtMl0Xn',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/07/18/16/20190718161706.png?itok=nCKA5oP7',
        ],
        age: 3,
        breed: ["Terrier", "Pit Bull", "Akita"]
    },
    {
        id: "2",
        code: "SSHS-A-12493",
        name: "Tango",
        description: ``,
        notes: `Heartworm Positive`,
        sex: "male",
        homeUrl: 'https://www.shelterluv.com/publish_animal/SSHS-A-12493?embedded=1&columns=2&iframeId=shelterluv_embed_2661561401138700&saved_query=3476#https://www-southsuburbanhumane-org.filesusr.com/html/a5d6fe_216e31003955bb7662a2d78c9461a8bf.html',
        avatarUrl: "https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/07/18/16/20190718161718.png?itok=EQ-NeLWB",
        photoUrls: [
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/12815/2019/04/29/07/20190429075621.png?itok=MHXOzYgm',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/12815/2019/05/15/14/20190515141549.png?itok=efWhlazQ',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/12815/2019/05/24/06/20190524062632.png?itok=4-Fv-RRF',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/12815/2019/05/24/06/20190524062645.png?itok=SsJvtoJF',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/12815/2019/06/27/09/20190627093607.png?itok=WAxUAy4t',
        ],
        age: 3,
        breed: ["Terrier", "American Staffordshire", "Labrador"],
        lbs: 56,
    },
    {
        id: "3",
        code: "SSHS-A-11749",
        name: "Simbole",
        description: `
        This beautiful gal is Simbole and she is a roughly 3 year old catahoula leopard mix! Her ideal home would be one with a fenced in yard and an active family to match her high energy. She has met other dogs her size and done well with them, but doesn't care for small dogs and probably would not be a great fit for a home with small dogs/cats due to her breed. She has met a few families with children and done really well with them!! She can be a bit anxious when left alone, so if you are home all day, that would be GREAT, but if not, a crate would be a great and safe place for her to spend the day!
        `,
        notes: `HAC, Heartworm Positive`,
        sex: "female",
        homeUrl: 'https://www.shelterluv.com/publish_animal/SSHS-A-11749?embedded=1&columns=2&iframeId=shelterluv_embed_2661561401138700&saved_query=3476#https://www-southsuburbanhumane-org.filesusr.com/html/a5d6fe_216e31003955bb7662a2d78c9461a8bf.html',
        avatarUrl: "https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/10/18/14/20191018140413.png?itok=O0ljIsDY",
        photoUrls: [
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/10/18/14/20191018140448.png?itok=GomSYX-4',
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/10/18/14/20191018140512.png?itok=XJDXlU64',
        ],
        age: 3,
        breed: ["Catahoula", "Leopard"],
        lbs: 50,
    },
    {
        id: "4",
        code: "SSHS-A-13395",
        name: "Corny",
        description: `
        Meet Corny! This handsome man is a super sweet boy who is ready for his forever home. Corny was lucky enough to go for a sleepover with one of our volunteers. Corny didn't have any accidents, did very well on leash, and "Was an absolute pleasure to spend the day with!". Corny has also done very well with other large breed dogs.
        `,
        sex: "male",
        homeUrl: 'https://www.shelterluv.com/publish_animal/SSHS-A-13395?embedded=1&columns=2&iframeId=shelterluv_embed_2661561401138700&saved_query=3476#https://www-southsuburbanhumane-org.filesusr.com/html/a5d6fe_216e31003955bb7662a2d78c9461a8bf.html',
        avatarUrl: "https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/10/12/05/20191012052102.png?itok=PeIRX3jI",
        photoUrls: [
            'https://www.shelterluv.com/sites/default/files/styles/large_square/public/animal_pics/266/2019/11/27/14/20191127143321.png?itok=E1DFXQjG',
        ],
        age: 4,
        breed: ["Terrier", "Pit Bull"],
        lbs: 60,
    },
]