import { v4 as uuid } from 'uuid';
import { MockLocation, MOCK_EMPTY_LOCATION } from './location';
import { MockDog, MOCK_EMPTY_DOG } from './dogs';
import { MockHub } from './hubs';

export interface MockContract {
    id: string;
    name: string;
    description?: string;
    url: string;
}

export interface MockShelter {
    id: string;
    name: string;
    description: string;
    location?: MockLocation;
    dogs: MockDog[];
    hubs: MockHub[];
    contracts: MockContract[]; // array of urls
    
}

export const MOCK_EMPTY_SHELTER: MockShelter = {
    id: uuid(),
    name: '',
    description: '',
    location: MOCK_EMPTY_LOCATION,
    dogs: [],
    hubs: [],
    contracts: [] // array of urls
};