import gql from 'graphql-tag';

const IS_LOGGED_IN = gql`
	query IsUserLoggedIn {
		isLoggedIn @client
	}
`;

// 'user' is caches in the local apollo client when user logs in
// this is a local only query as indicated by the @client directive
const GET_LOCAL_USER = gql`
	query GetUser {
		user @client {
			id
			name
			email
			roles
		}
	}
`;

const GET_ME = gql`
	query getMe {
		me {
			id
			name
			email
			roles
		}
	}
`;

const GET_HUB_SUMMARIES = gql`
	query hubSummaryList {
		hubSummaries {
			id
			name
			open
			close
		}
	}
`;

const GET_HUB_DETAILS = gql`
	query GetHubDetails($hubId: ID!) {
		hub(id: $hubId) {
			id
			name
			locations {
				location {
					id
					line1
					line2
					line3
					city
					region
					postcode
					country
					web
					phone
					fax
					latitude
					longitude
				}
			}
			schedule {
				id
				notes
				open
				close
				schedule {
					id
					dog {
						id
						name
						description
						sex
						avatarUrl
						headerUrl
						breed
						age
					}
					walks {
						id
						start
						stop
						user {
							id
						}
					}
				}
			}
			shelter {
				id
				name
				description
				manager {
					id
					name
					email
					locations {
						location {
							phone
						}
					}
				}
				dogs {
					id
					name
					description
					sex
					avatarUrl
					headerUrl
					breed
					age
				}
				contracts {
					id
					title
					filename
					url
					mime
					required
				}
				locations {
					location {
						id
						line1
						line2
						line3
						city
						region
						postcode
						country
						web
						phone
						fax
						latitude
						longitude
					}
				}
			}
		}
	}
`;

const GET_SHELTERS = gql`
	query shelterList {
		shelters {
			id
			name
			locations {
				location {
					id
					line1
					city
					region
					postcode
				}
			}
		}
	}
`;

export {
	IS_LOGGED_IN,
	GET_LOCAL_USER,
	GET_ME,
	GET_HUB_SUMMARIES,
	GET_HUB_DETAILS,
	GET_SHELTERS,
};
