import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { resolvers, typeDefs } from '../localstate/resolvers';

const httpLink = createHttpLink({
	uri: process.env.REACT_APP_APOLLO_URI,
});

const authLink = setContext((_, { headers }) => {
	// get the authentication token from local storage if it exists
	const token = localStorage.getItem('token');
	// return the headers to the context so httpLink can read them
	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : '',
		},
	};
});

const cache = new InMemoryCache();

const apollo = new ApolloClient({
	link: authLink.concat(httpLink),
	cache,
	typeDefs,
	resolvers,
});

cache.writeData({
	data: {
		isLoggedIn: !!localStorage.getItem('token'),
		cartItems: [], // TODO: useful?
	},
});

export { apollo };
