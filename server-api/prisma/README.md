# README

## Prisma

Start prisma server

```
sudo docker-compose up -d
```

Deploy your prisma service

```
prisma deploy
```

Wipe all the data but keep current schema

```
prisma reset
```

Delete the data, schema and start completely from scratch

```
prisma delete
```

## psql

List databases

```
\l
```

Change databases

```
\c fostershare-staging
```

List schemas in current database

```
\dn
```

List table search path

```
SHOW search_path;
```

Add your schema to the search path (so your tables are visible)

```
SET search_path TO default$default;
```

Describe all tables

```
\d
```

Select from some table

```
SELECT * FROM "User";

```

Describe table columns:

```
\d "User"
```
