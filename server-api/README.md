# README

Build dev docker image

```
sudo docker build -f Dockerfile.dev .
```

Run dev docker image

```
sudo docker run <image-id>
```
