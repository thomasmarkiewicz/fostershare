import { Prisma } from 'prisma-binding';

const prisma = new Prisma({
  typeDefs: 'src/generated/prisma.graphql',
  endpoint: 'https://fostershare.app/QBHFpe3hXvQrZKNk32Hnq6No1LHKAOKh2bE25zT'
});

prisma.query.users(null, '{ id email posts {id title} }').then(data => {
  console.log(JSON.stringify(data, undefined, 2));
});
