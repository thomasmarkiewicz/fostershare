//require('@babel/register');
//require('@babel/polyfill/noConflict');

import 'core-js/stable';
import 'regenerator-runtime/runtime';

const server = require('../../src/server').server;

module.exports = async () => {
	global.httpServer = await server.start({ port: 4000 });
};
