import 'cross-fetch/polyfill';
import ApolloBoost, { gql } from 'apollo-boost';

const client = new ApolloBoost({
	uri: 'http://localhost:4000',
});

beforeEach(async () => {});

test('should return welcome message', async () => {
	const getMsg = gql`
		query {
			hello
		}
	`;
	const msg = await client.query({
		query: getMsg,
	});
	expect(msg).toBe('Humble beginning of the fostershare.app!!');
});
