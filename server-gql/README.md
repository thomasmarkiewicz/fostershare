# README

Start app for development

```
npm run start
```

Build dev docker image

```
sudo docker build -f Dockerfile.dev .
```

Run dev docker image

```
sudo docker run -p 4000:4000 <image-id>
```

Or run with docker-compose

```
sudo docker-compose up --build
```

When happy with the results, build release/deployment version tagging it. Then update the version tag in ../k8s/server-gql-deployment.yml version (for manual deployments until CI/CD is setup)

```
sudo docker build -t thomasmarkiewicz/fostershare-server-gql .
sudo docker build -t thomasmarkiewicz/forestshare-server-gql:0.0.1 .
sudo docker push thomasmarkiewicz/fostershare-server-gql:latest
sudo docker push thomasmarkiewicz/forestshare-server-gql:0.0.1
# update ../k8s/server-gql-deployment.yml version
kubectl apply -f k8s
```
