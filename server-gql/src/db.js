const popups = [
	{
		id: '1',
		description: 'Popup one',
	},
	{
		id: '12',
		description: 'Popup two',
	},
	{
		id: '3',
		description: 'Popup three',
	},
];

const db = {
	popups,
};

export { db };
