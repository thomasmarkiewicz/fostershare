import { Prisma } from 'prisma-binding';

const prisma = new Prisma({
	typeDefs: process.env.PRISMA_TYPEDEFS,
	endpoint: process.env.PRISMA_ENDPOINT,
});

export { prisma };
