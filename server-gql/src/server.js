import '@babel/polyfill';
import { GraphQLServer, PubSub } from 'graphql-yoga';
import { db } from './db';
import { Query } from './resolvers/Query';
import { Mutation } from './resolvers/Mutation';
import { User } from './resolvers/User';
import { prisma } from './prisma';

const pubsub = new PubSub();

const PRODUCTION_TYPEDEFS = './schema.graphql';
const DEV_TYPEDEFS = './src/schema.graphql';

// server
const server = new GraphQLServer({
	typeDefs: process.env.SERVER_TYPEDEFS,
	resolvers: {
		Query,
		Mutation,
		User,
	},
	context(request) {
		return {
			db,
			pubsub,
			prisma,
			request,
		};
	},
});

export { server };
