import jwt from 'jsonwebtoken';

const JWT_SECRET = process.env.JWT_SECRET;

const getUserId = (request, role) => {
	const authHeader = request.request.headers.authorization;

	if (!authHeader) {
		throw new Error('Authentication required');
	}

	const token = authHeader.replace('Bearer ', '');
	const decoded = jwt.verify(token, JWT_SECRET);

	if(role && !decoded.roles.includes(role)){
		throw new Error('Not authorized');
	}

	return decoded.userId;
};

export { JWT_SECRET, getUserId };
