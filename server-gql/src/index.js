import { server } from './server';

server.start({ port: 4000 }, () => {
	console.log('server-gql is up on port 4000');
});
