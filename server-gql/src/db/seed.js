//import { Prisma } from 'prisma-binding';

const pb = require('prisma-binding');
var faker = require('faker');
var bcrypt = require('bcryptjs');

const prisma = new pb.Prisma({
	typeDefs: 'src/generated/prisma.graphql',
	endpoint: 'http://localhost:4466',
});

const createLocation = async data => {
	const loc = await prisma.mutation.createLocation(
		{
			data,
		},
		`{
			id
			line1
			line2
			line3
			city
			region
			postcode
			country
			web
			phone
			fax
			latitude
			longitude
		}`,
	);
	console.log(JSON.stringify(loc, undefined, 2));
};

const createRole = async ({ name, description }) => {
	/*
mutation {
  createRole(data: {name: "admin", description: "admin desc"}){id name description}
}
	*/
	const role = await prisma.mutation.createRole(
		{
			data: {
				name,
				description,
			},
		},
		'{id name description}',
	);
	console.log(JSON.stringify(role, undefined, 2));
};

const createUser = async ({ name, email, password, role }) => {
	const user = await prisma.mutation.createUser(
		{
			data: {
				name,
				email,
				password: await bcrypt.hash(password, 10),
				roles: {
					connect: {
						id: role,
					},
				},
			},
		},
		`{id name email}`,
	);
	console.log(JSON.stringify(user, undefined, 2));
};

const createDog = async ({ name, description, breed, sex }) => {
	const dog = await prisma.mutation.createDog(
		{
			data: {
				name,
				description,
				breed,
				sex,
			},
		},
		'{ id name description breed sex }',
	);
	console.log(JSON.stringify(dog, undefined, 2));
};

const createShelter = async ({ name, description, manager, dog }) => {
	const shelter = await prisma.mutation.createShelter({
		data: {
			name,
			description,
			manager: {
				connect: {
					email: manager,
				},
			},
			dogs: {
				connect: {
					id: dog,
				},
			},
			locations: {
				create: {
					location: {
						create: {
							line1: '1103 West End Ave',
							city: 'Chicago Heights',
							region: 'IL',
							postcode: '60411',
							country: 'US',
							web: 'southsuburbanhumanesociety.org',
							phone: '(708) 755-7387',
							fax: '',
							latitude: 41.5114057,
							longitude: -87.6329856,
						},
					},
				},
			},
		},
	});
	console.log(JSON.stringify(shelter, undefined, 2));
};

const createHub = async ({
	name,
	shelter,
	manager,
	location,
	schedule,
}) => {

	const hub = await prisma.mutation.createHub({
		data: {
			name: name,
			shelter: {
				connect: {
					id: shelter,
				},
			},
			manager: {
				connect: {
					email: manager,
				},
			},
			locations: {
				create: {
					location: {
						create: location,
					},
				},
			}
		},
	});

	console.log(JSON.stringify(hub, undefined, 2));

	schedule.forEach(async s => {
		const hubDay = await prisma.mutation.createHubDay({
			data: {
				open: s.open,
				close: s.close,
				hub: {
					connect: {
						id: hub.id
					}
				},
				schedule: {
					create:{
			
					}
				  }
			}
		})
	})
};

const createRoles = async () => {
	console.log('CREATING ROLES');
	await createRole({
		name: 'admin',
		description: 'Manage shelters, hubs, dogs, ...everything',
	});
	await createRole({
		name: 'walker',
		description: 'Schedule and manage their own dog walks',
	});
};

const createUsers = async () => {
	console.log('CREATING USERS');

	/*
		query {
			roles(where: {name: "admin"}) {
				id
				name
				description
			}
		}
	*/
	const adminRoles = await prisma.query.roles(
		{ where: { name: 'admin' } },
		'{id name description }',
	);
	console.log('adminRoles', adminRoles[0].id);
	const adminRole = adminRoles[0].id;

	const walkerRoles = await prisma.query.roles(
		{ where: { name: 'walker' } },
		'{id name description }',
	);
	console.log('walkerRoles', walkerRoles[0].id);
	const walkerRole = walkerRoles[0].id;

	await createUser({
		name: 'Nicole Quattrocki',
		email: 'nquattrocki@icloud.com',
		password: 'fostershare!',
		role: adminRole,
	});

	await createUser({
		name: 'Tom',
		email: 'thomas.markiewicz@protonmail.com',
		password: 'fostershare!',
		role: adminRole,
	});

	await createUser({
		name: 'Ram Prasad',
		email: 'ramprasad.seeda@gmail.com',
		password: 'fostershare!',
		role: adminRole,
	});

	await createUser({
		name: 'Vaughan',
		email: 'vaughan@emotiveintelligence.com',
		password: 'fostershare!',
		role: adminRole,
	});

	await createUser({
		name: 'Tomek',
		email: 'tomek@librem.one',
		password: 'fostershare!',
		role: walkerRole,
	});

	await createUser({
		name: 'Admin',
		email: 'admin@fostershare.app',
		password: 'fostershare!',
		role: adminRole,
	});

	await createUser({
		name: 'Walker',
		email: 'walker@fostershare.app',
		password: 'fostershare!',
		role: walkerRole,
	});
};

const createDogs = async () => {
	console.log('CREATING DOGS');
	await createDog({
		name: 'Enzo',
		description: 'Loves cats and bunnies (eating them that is ;)',
		breed: 'Doberman',
		sex: 'male',
	});
};

const createLocations = async () => {
	// maps: https://www.google.com/maps/place/South+Suburban+Humane+Society/@41.5114057,-87.6329856,17z/data=!4m12!1m6!3m5!1s0x880e19442ce43d23:0xc8144b9ee484e614!2sSouth+Suburban+Humane+Society!8m2!3d41.5114017!4d-87.6307969!3m4!1s0x880e19442ce43d23:0xc8144b9ee484e614!8m2!3d41.5114017!4d-87.6307969
	// 1103 West End Ave, Chicago Heights, IL 60411
	// web: southsuburbanhumanesociety.org
	// phone: (708) 755-7387

	/*
mutation {
  createLocation(
    data: {
      line1: "1103 West End Ave",
      city: "Chicago Heights",
      region: "IL",
      postcode: "60411",
      country: "US"
      web: "southsuburbanhumanesociety.org",
      phone: "(708) 755-7387",
      fax: "",
      latitude: 41.5114057,
      longitude: -87.6329856,
    }
  ) {
    id
    line1
    line2
    line3
    city
    region
    postcode
    country
    web
    phone
    fax
    latitude
    longitude
  }
}
	*/

	console.log('CREATING LOCATIONS');

	await createLocation({
		line1: '1103 West End Ave',
		city: 'Chicago Heights',
		region: 'IL',
		postcode: '60411',
		country: 'US',
		web: 'southsuburbanhumanesociety.org',
		phone: '(708) 755-7387',
		fax: '',
		latitude: 41.5114057,
		longitude: -87.6329856,
	});
};

const createShelters = async () => {
	console.log('CREATING SHELTERS');

	const dogs = await prisma.query.dogs({ first: 1 }, '{id}');

	await createShelter({
		name: 'South Suburban Humane Society',
		description: faker.lorem.paragraph(3),
		manager: 'thomas.markiewicz@protonmail.com',
		dog: dogs[0].id,
	});
};

const createHubs = async () => {
	console.log('CREATING HUBS');

	const shelters = await prisma.query.shelters(
		{ first: 1 },
		'{id dogs { id } }',
	);
	const shelter = shelters[0].id;

	await createHub({
		name: 'Mt. Greenwood Park Fostershare Hub',
		shelter,
		manager: 'nquattrocki@icloud.com',
		location: {
			line1: '3721 W 111th St',
			city: 'Chicago',
			region: 'IL',
			postcode: '60655',
			country: 'US',
			web: '',
			phone: '',
			fax: '',
			latitude: 41.6895624,
			longitude: -87.71584,
		},
		schedule: [
			{
				open: '2020-05-23T09:00:00',
				close: '2020-05-23T18:00:00',
			},
			{
				open: '2020-05-30T09:00:00',
				close: '2020-05-30T18:00:00',
			},
			{
				open: '2020-06-06T09:00:00',
				close: '2020-06-06T18:00:00',
			},
		],
	});
};

const createAll = async () => {
	await createRoles();
	await createUsers();
	await createDogs();
	//await createLocations();
	await createShelters();
	await createHubs();
};

createAll().then(() => {
	console.log('ALL done');
});
