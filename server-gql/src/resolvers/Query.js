import { getUserId, JWT_SECRET } from '../utils';

const Query = {
	hello() {
		return `Humble beginning of the fostershare.app!!!`;
	},
	async me(parent, args, ctx, info) {
		const { prisma, request } = ctx;
		const userId = getUserId(request);

		const users = await prisma.query.users(
			{ where: { id: userId }, first: 1 },
			'{ id name email roles { name } }',
		);

		const me = {
			...users[0],
			roles: users[0].roles.map(r => r.name),
		};

		return me;
	},
	async hubSummaries(parent, args, ctx, info) {
		const { prisma } = ctx;

		const opArgs = {};

		// OR for example purposes only
		if (args.query) {
			opArgs.where = {
				OR: [
					{
						name_contains: args.query,
					},
					{
						name_contains: 'something bogus',
					},
				],
			};
		}

		const hubs = await prisma.query.hubs(
			{},
			'{id, name, schedule {open close}}',
		);

		const summaries = hubs.map(h => ({
			id: h.id,
			name: h.name,
			open: (h.schedule && h.schedule[0] && h.schedule[0].open) || '',
			close: (h.schedule && h.schedule[0] && h.schedule[0].close) || '',
		}));

		return summaries;
	},
	hub(parent, args, ctx, info) {
		const { prisma } = ctx;

		const opArgs = {
			where: {
				id: args.id
			}
		};

		return prisma.query.hub(opArgs, info);
	},
	hubs(parent, args, ctx, info) {
		const { prisma } = ctx;

		const opArgs = {};

		// OR for example purposes only
		if (args.query) {
			console.log('args.query', args.query);
			opArgs.where = {
				id: args.query,
			};
		}	

		console.log('args', args);

		console.log('info', info);
		return prisma.query.hubs(args, info);
	},
	shelters(parent, args, ctx, info) {
		const { prisma } = ctx;

		const opArgs = {};

		// OR for example purposes only
		if (args.query) {
			opArgs.where = {
				OR: [
					{
						name_contains: args.query,
					},
					{
						name_contains: 'something bogus',
					},
				],
			};
		}

		return prisma.query.shelters(opArgs, info);
	},
};

export { Query };
