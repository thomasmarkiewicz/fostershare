import uuidv4 from 'uuid/v4';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { getUserId, JWT_SECRET } from '../utils';

const Mutation = {
	async registerUser(parent, args, ctx, info) {
		const { prisma } = ctx;
		const { name, email, password, phone } = args.data;

		const role = await prisma.query.role(
			{
				where: {
					name: 'walker',
				},
			},
			'{id}',
		);

		if (!role) {
			throw new Error('Unable to register a walker');
		}

		const data = {
			name,
			email,
			password,
			password: await bcrypt.hash(args.data.password, 10),
			locations: {
				create: {
					location: {
						create: {
							phone,
						},
					},
				},
			},
			roles: {
				connect: {
					id: role.id
				}
			}
		};

		const user = await prisma.mutation.createUser({ data });

		const roles = ['walker'];

		return {
			user: { id: user.id, name: user.name, email: user.email, roles },
			token: jwt.sign({ userId: user.id, roles: roles.join(' ') }, JWT_SECRET),
		};

	},

	async loginUser(parent, args, ctx, info) {
		const { prisma } = ctx;

		const user = await prisma.query.user(
			{
				where: {
					email: args.data.email,
				},
			},
			'{id name email password roles { name } }',
		);

		if (!user) {
			throw new Error('Unable to log in');
		}

		const isMatch = await bcrypt.compare(args.data.password, user.password);

		if (!isMatch) {
			throw new Error('Unable to log in');
		}

		const roles = user.roles.map(r => r.name);

		return {
			user: { id: user.id, name: user.name, email: user.email, roles },
			token: jwt.sign({ userId: user.id, roles: roles.join(' ') }, JWT_SECRET),
		};
	},

	async deleteUser(parent, args, ctx, info) {
		const { prisma } = ctx;
		return prisma.mutation.deleteUser(
			{
				where: {
					id: args.id,
				},
			},
			info,
		);
	},

	async createDog(parent, args, ctx, info) {
		const { prisma, request } = ctx;

		const userId = getUserId(request, 'admin');

		return prisma.mutation.createDog({
			data: args.data,
		});
	},
};

export { Mutation };
