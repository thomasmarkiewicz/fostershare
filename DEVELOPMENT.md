# DEVELOPMENT

The purpose of this file is to describe how to setup a local development evironment for both the back-end and front-end development.

The solution setup and deployment can be rather intimidating, but is really not that bad thanks to [docker](https://www.docker.com).

The instructions below assume you have it installed on your local development machine.

## LAYERS

The "development" solution consists of several layers:

- back-end [Postgres](https://www.postgresql.org/) database (related: [pgAdmin](https://www.pgadmin.org/))
- back-end [Prisma](http://prisma.io/) server
- back-end [GraphQL Yoga](https://github.com/prisma-labs/graphql-yoga) server (uses Prisma client to access the datase)
- front-end [reactjs](https://reactjs.org/) single page web app client (uses [Apollo](https://www.apollographql.com/) to access the back-end GraphQL Yoga server)
- [nginx](https://www.nginx.com/) proxy (serves content on localhost:3050)

## TOOLS

These are the tools you need to install globally on your system.

For local development:

- [nvm](https://github.com/nvm-sh/nvm)
- [yarn](https://yarnpkg.com/lang/en/docs/install/#debian-stable)
- [typescript](https://www.typescriptlang.org/#download-links)
- [docker](https://docs.docker.com/v17.12/install/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [prisma cli](https://www.prisma.io/docs/1.34/get-started/01-setting-up-prisma-new-database-JAVASCRIPT-a002/#install-the-prisma-cli)
   - NOTE: re-install if you get errors like: 'prisma.yml should NOT have additional properties. additionalProperty: endpoint'
- [pgadmin](https://www.pgadmin.org/download/) (_OPTIONAL_)

For production deployment only (not needed for local development):

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/inst)
- [doctl](https://github.com/digitalocean/doctl#installing-doctl)

## STARTING, DEVELOPING, AND STOPPING

All of the above have been preconfigured in [docker-compose.yml](./docker-compose.yml) file and should be easy to install, start, and stop on your local development machine regardless of what OS you're running: Linux, Mac, or Windows. The pre-requisite is that you have Docker installed, that's it!


To start ALL of the above servers, at the root of the repo directory type:

```
sudo docker-compose up --build
```

This will crunch a while so be patient. It downloads and installs the database, servers, etc. into docker components of your local machine. When it's done:

First time around you may need to generate the database schema:

```
cd server-gql/prisma
prisma deploy
npm run get-schema
```

To see and manually add data to the database, point your browser to:

```
http://localhost:4466/_admin
http://localhost:4466/
```

To seed the database with test data:

```
cd server-gql
npm run db:seed
```

To delete all database data:

```
cd server-gql
prisma reset -f
```

To delete the data AND schema, and start completely from scratch

```
prisma delete
```

To see the web app in your browser point it to:

```
http://localhost:3050
```

You can now start making changes to the sources in the `webapp` subfolder, and the browser should refresh and reflect your changes as soon as you save the file.

The web app gets its data from the server-gql. server-gql eposes a GraphQL enpoint with schema and documentation that can be explored visually in the Playground:

```
http://localhost:4000/
```

When done for the day, you can shut down everything by pressing CTRL-D.

Or if you started docker-compose as a daemon, shut it down with:

```
sudo docker-compose down
```

## psql

List databases

```
\l
```

Change databases

```
\c fostershare-staging
```

List schemas in current database

```
\dn
```

List table search path

```
SHOW search_path;
```

Add your schema to the search path (so your tables are visible)

```
SET search_path TO default$default;
```

Describe all tables

```
\d
```

Select from some table

```
SELECT * FROM "User";

```

Describe table columns:

```
\d "User"
```
