# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased] 

## [0.0.10] - 2020-02-02
### Added
- Fetching and showing "some" real data for the Hub details screen

## [0.0.9] - 2020-02-02
### Added
- New user registration (full stack)
- This CHANGELOG.md ;)
### Changed
- Going forward both webapp and server will be deployed at the same time and have same version numbers with changes kept in this CHANGELOG
- Deleted prod db and seeded with new data
### Security
- so far JWT_SECRET secret has been exposed in source file which is not secure
- going forward, in prod environmet, it is stored as a Kubernetes secret and exposed as an environmet variable to server-gql
- for dev environment it is saved in .env file and its values is different from prod
- The prod secret saved in Keepass file
