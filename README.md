# README

This is a repository for everything needed to develop and deploy fostershare.app to both your development machine as well as production.

Read [DEVELOPMENT.md](./DEVELOPMENT.md) to setup for local developmet.

Read [PRODUCTION.md](./PRODUCTION.md) to learn about production development to the Kubernetes cluster in Digital Ocean.
